#include <iostream>
#include <cstring>
#include "string"
using namespace std;


class HashTable
{
public:
    HashTable();
    void push( int years, string name );
    void pop( int years, string name );
    bool find( int years, string name );
private:
    int hash( int key );
    struct Node
    {
        int years;
        string name;
        Node * next;
        Node( int years, string name): years(years), name(name), next(nullptr){};
    };
    Node ** buffer;
};

HashTable::HashTable()
{
    buffer = new Node* [100];
    for( int i = 0; i < 100; ++i )
        buffer[i] = nullptr;
}

int HashTable::hash( int key )
{
    return key % 100;
}

void HashTable::push( int years, string name )
{
    int index = hash( years );

    if( buffer[index] == nullptr )
    {
        Node * newNode = new Node( years, name );
        buffer[index] = newNode;
        return;
    }

    Node * searcher = buffer[index];
    while ( searcher->next && strcmp(searcher->name.c_str(), name.c_str()) )
        searcher = searcher->next;

    if( strcmp(searcher->name.c_str(), name.c_str()) )
    {
        Node * newNode = new Node( years, name );
        searcher->next = newNode;
    }
}

bool HashTable::find(int years, string name)
{
    int index = hash( years );
    Node * searcher = buffer[index];
    while ( searcher->next && strcmp(searcher->name.c_str(), name.c_str()) )
        searcher = searcher->next;

    if( !strcmp(searcher->name.c_str(), name.c_str()) )
        return true;
    else
        return false;
}

void HashTable::pop( int years, string name )
{
    int index = hash( years );
    Node * searcher = buffer[index];
    Node * prev = nullptr;
    while ( searcher->next && strcmp(searcher->name.c_str(), name.c_str()) )
    {
        prev = searcher;
        searcher = searcher->next;
    }

    if( !strcmp(searcher->name.c_str(), name.c_str()) && prev != nullptr )
    {
        prev->next = searcher->next;
        delete searcher;
    }
    if( !strcmp(searcher->name.c_str(), name.c_str()) && prev == nullptr )
    {
        buffer[index] = buffer[index]->next;
        delete searcher;
    }
}

int main() {
    HashTable ht ;
    ht.push(2,"petya");
    ht.push(3,"v1");
    ht.push(4,"v2");
    ht.push(2,"v3");
    ht.push(2,"v4");

    ht.pop(2,"v4");

    std::cout << ht.find(2,"v4") << "  ";
    std::cout << ht.find(2,"petya") << "  ";
    std::cout << ht.find(2,"v6");
    return 0;
}

﻿#include <iostream>
#include <vector>
#include <cassert>
using namespace std;

class HasherSTR {
public:
	unsigned int operator()(const string& value) const {
		unsigned int hash = 0;
		for (int i = 0; i < size(value); i++) {
			hash = hash * 10 + value[i];
		}
		return hash;
	}
	unsigned int operator()(const int& lasthash, const int stepcount) const {

		return lasthash + stepcount % 64;
	}
};

template<class T, class Hasher>
class HashTable {
private:
	enum status {
		DEL,
		EMPTY,
		FULL,

	};

	struct Node
	{
		T value;
		status flag;
		Node() :flag(EMPTY) {};
		Node(const T& value, status flag) :value(value), flag(flag) {};
	};

	void grow();

	vector<Node> buffer;
	int count;
	Hasher hasher;
public:
	HashTable();
	void Add(const T& value);
	void Dell(const T& value);
	bool Find(const T& value) const;
};

template<class T, class Hasher>
bool HashTable<T, Hasher>::Find(const T& value) const {
	int index = hasher(value) % buffer.size();
	int stepcount = 0;
	while (buffer[index].flag != EMPTY && stepcount++ < buffer.size()) {
		if (buffer[index].flag == FULL && buffer[index].value == value)
			return true;
		index = hasher(index, stepcount) % buffer.size();
	}
	return false;
}

template<class T, class Hasher>
void HashTable<T, Hasher>::Dell(const T& value) {
	int index = hasher(value) % buffer.size();
	int stepcount = 0;
	while (buffer[index].flag != EMPTY && stepcount++ < buffer.size()) {
		if (buffer[index].flag == FULL && buffer[index].value == value) {
			buffer[index].flag = DEL;
			count--;
			return;
		}
		index = hasher(index, stepcount) % buffer.size();
	}

}

template<class T, class Hasher>
void HashTable<T, Hasher>::grow() {
	vector<Node> oldBuffer = buffer;
	buffer = vector<Node>(2 * buffer.size());
	count = 0;
	for (int i = 0; i < oldBuffer.size(); i++) {
		if (oldBuffer[i].flag == FULL) {
			Add(oldBuffer[i].value);
		}
	}
}

template<class T, class Hasher>
void HashTable<T, Hasher>::Add(const T& value) {
	if (buffer.size() * 0.75 < count) {
		grow();
	}
	int firstdel = -1;
	int index = hasher(value) % buffer.size();
	int stepcount = 0;
	while (buffer[index].flag != EMPTY && stepcount++ < buffer.size()) {
		if (buffer[index].flag == DEL && firstdel == -1)
			firstdel = index;
		if (buffer[index].flag == FULL && buffer[index].value == value)
			return;
		index = hasher(index, stepcount) % buffer.size();
	}

	if (buffer[index].flag = EMPTY) {
		if (firstdel != -1) {
			buffer[firstdel].value = value;
			buffer[firstdel].flag = FULL;
		}
		else {
			buffer[index].value = value;
			buffer[index].flag = FULL;
			count++;
		}
	}
	else {
		assert(firstdel != -1);
		buffer[firstdel].value = value;
		buffer[firstdel].flag = FULL;
	}

}

template<class T, class Hasher>
HashTable<T, Hasher>::HashTable():buffer(8), count(0){

}

int main()
{
HashTable<string, HasherSTR> hst;
hst.Add("aaaaaa");
hst.Add("abbbb");
hst.Add("adafd");
hst.Add("affef");
hst.Add("fgfsfds");
hst.Add("aaaaaa");
hst.Add("bbbbbbbbbbbb");
hst.Add("ssss");
hst.Add("ccccbbbbbbbb");

hst.Dell("affef");
cout << hst.Find("affef") << endl;
cout << hst.Find("aaaaaa") << endl;
return 0;
}

